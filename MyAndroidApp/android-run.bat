echo "Exportando variaveis de ambiente"
set JAVA_HOME="C:\Arquivos de programas\Java\jdk1.7.0_79"
set ANDROID_HOME="C:\Documents and Settings\Administrador\AppData\Local\Android\sdk"

echo "Criando variaveis de configuracao"
set SDKVERSION=21.1.2
set MAINCLASS=br\edu\ifpb\pdm\example\MainActivity.java

echo "Gerando a classe de recursos R.java"
%ANDROID_HOME%\build-tools\%SDKVERSION%\aapt package -v -f -m -S res -J src -M AndroidManifest.xml -I %ANDROID_HOME%\platforms\android-8\android.jar

echo "Compilando os arquivos java"
%JAVA_HOME%\bin\javac -verbose -d java-bin -classpath %ANDROID_HOME%\platforms\android-8\android.jar;java-bin -sourcepath src src\%MAINCLASS%

echo "Convertendo para o formato DEX"
%ANDROID_HOME%\build-tools\%SDKVERSION%\dx --dex --verbose --output=bin\classes.dex java-bin libs